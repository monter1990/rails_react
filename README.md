# Ruby on Rails project set up to React using gem react-on-rails following this tutorial https://github.com/shakacode/react_on_rails/blob/master/docs/tutorial.md

* ruby '2.6.3'
* 'rails', '~> 5.2.3'
* 'react_on_rails', '11.2.1'

To compile from this repo, run:<br>
`git clone <url>`<br>
`cd rails_react`<br>
`bundle & yarn`<br>
`rake db:setup`<br>
`rake db:migrate`<br>
`foreman start`<br>
`pg_ctl -D /usr/local/var/postgres start && brew services start postgresql`<br>
http://localhost:5000/

#### This is how this project's done from scratch<br>
`rails new <name_of_the_project> --webpack=react`<br>
`rails new <name> [--webpack=WEBPACK]`  # Preconfigure for app-like JavaScript with Webpack (options: react/vue/angular/elm/stimulus)

Add the [react-on-rails](https://github.com/shakacode/react_on_rails) gem to your **/Gemfile**:<br>
*gem 'react_on_rails', '11.2.1'* **# prefer exact gem version to match npm version**

Run:<br>
`bundle & yarn`

and commit the git repository (or `rails generate` will not work properly)<br>
`git add -A`<br>
`git commit -m "Initial commit"`

Then run:<br>
`rails generate react_on_rails:install`

this should run `bundle` and `yarn` if it doesn't run manually

after that create **/Procfile** in root of the application. Paste this line<br>
*web: bundle exec rails server -p $PORT*

foreman defaults to PORT 5000 unless you set the value of PORT in your environment or in the Procfile.<br>
`foreman start`

go to http://localhost:5000/hello_world

Additionally add this line to ./config/routes.rb<br>
*root "hello_world#index"*

if done so, react will be routed to index of your localhost
http://localhost:5000/
